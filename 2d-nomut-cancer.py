from pyomo.environ import *
from pyomo.dae import *
from matplotlib import pyplot as plt
import numpy as np

model = ConcreteModel(name="ad")

model.tf = Param(initialize=13.5)
model.t = ContinuousSet(bounds=(0, model.tf))

model.u = Var(model.t, initialize=0)

model.x1 = Var(model.t, domain=NonNegativeReals)
model.x2 = Var(model.t, domain=NonNegativeReals)

model.z = Var(model.t, domain=NonNegativeReals)

model.gam1 = Param(initialize=0.192)
model.gam2 = Param(initialize=model.gam1/2)

model.eta1 = Param(initialize=3)
model.eta2 = Param(initialize=3)
model.omega1 = Param(initialize=60)
model.omega2 = Param(initialize=60)
model.xi = Param(initialize=0.5)
model.eps = Param(initialize=0.01)

model.dx1 = DerivativeVar(model.x1, wrt=model.t)
model.dx2 = DerivativeVar(model.x2, wrt=model.t)
model.zdot = DerivativeVar(model.z, wrt=model.t)

def _x1dot(m, t):
    if t == m.t.first():
        return Constraint.Skip

    return m.dx1[t] == m.gam1 * m.x1[t] * (1 - m.x1[t] - m.x2[t]) - m.x1[t] * m.u[t]

def _x2dot(m, t):
    if t == m.t.first():
        return Constraint.Skip

    return m.dx2[t] == m.gam2 * m.x2[t] * (1 - m.x1[t] - m.x2[t])

def _zdot(m, t):
    if t == m.t.first():
        return Constraint.Skip

    return m.zdot[t] == (m.eta1*m.x1[t] + m.eta2*m.x2[t] + m.xi*(1 + tanh((m.x2[t] - m.x1[t])/m.eps)))

def _ucon(m, t):
    return (m.u[t] >= 0 and m.u[t] <= 1)

model.dx1con = Constraint(model.t, rule=_x1dot)
model.dx2con = Constraint(model.t, rule=_x2dot)
model.dzcon = Constraint(model.t, rule=_zdot)
model.u_con = Constraint(model.t, rule=_ucon)

def _init(m):
    yield m.x1[m.t.first()] == 0.45
    yield m.x2[m.t.first()] == 0.05
    yield m.z[m.t.first()] == 0

model.init = ConstraintList(rule=_init)

model.obj = Objective(expr=model.omega1*model.x1[model.tf] + model.omega2*model.x2[model.tf] + model.z[model.tf])

discr = TransformationFactory('dae.finite_difference')
discr.apply_to(model, nfe=5000, wrt=model.t, scheme='BACKWARD')

solver = SolverFactory('ipopt', executable='./ipopt')
solver.options['max_iter'] = 1000
solver.options['tol'] = 1e-10

results = solver.solve(model, tee=True)

def plotter(subplot, x, *y, **kwds):
    plt.subplot(subplot)
    for i,_y in enumerate(y):
        plt.plot(list(x), [value(_y[t]) for t in x], 'brgcmk'[i%6])
        if kwds.get('points', False):
            plt.plot(list(x), [value(_y[t]) for t in x], 'o')
    plt.title(kwds.get('title',''))
    plt.legend(tuple(_y.name for _y in y))
    plt.xlabel(x.name)

plotter(121, model.t, model.x1, model.x2)
plotter(122, model.t, model.u)
plt.show()
